/**
 * Create time 2019-06-05 10:34
 * Create by kiilin kiilin@kiilin.com
 * Copyright 2019 kiilin http://www.kiilin.com
 */

package com.kiilin.sensitive.core.constant;

/**
 * 常量类，用于定义敏感信息处理相关的常量
 *
 * @author kiilin
 * @version V 1.0
 * @since JDK1.8
 */

public class SensitiveConstant {

    /**
     * 标记是否需要进行脱敏处理的常量
     */
    public static final String IS_SENSITIVE = "IS_SENSITIVE";
    /**
     * 用于替换敏感信息的占位符常量
     */
    public static final String SENSITIVE_PLACEHOLDER = "sensitive-placeholder";
}

